# -*- coding: utf-8 -*-

'''
NASTY: Neural Art Style Transfer with pYthon.
=============================================

Run the script with:
```
python neural_style_transfer.py path_to_your_base_image.jpg path_to_your_reference.jpg prefix_for_results
```
e.g.:
```
python neural_style_transfer.py img/tuebingen.jpg img/starry_night.jpg results/my_result
```

Optional parameters:
```
--iter, To specify the number of iterations the style transfer takes place (Default is 10)
```
```
--content_weight, The weight given to the content loss (Default is 0.025)
```
```
--style_weight, The weight given to the style loss (Default is 1.0)
```
```
--tv_weight, The weight given to the total variation loss (Default is 1.0)
```

It is preferable to run this script on GPU, for speed.


Details
-------

Style transfer consists in generating an image
with the same "content" as a base image, but with the
"style" of a different picture (typically artistic).

This is achieved through the optimization of a loss function
that has 3 components: "style loss", "content loss",
and "total variation loss":


- The total variation loss imposes local spatial continuity between the \
pixels of the combination image, giving it visual coherence.


- The style loss is where the deep learning keeps in --that one is defined \
using a deep convolutional neural network. Precisely, it consists in a sum of \
L2 distances between the Gram matrices of the representations of \
the base image and the style reference image, extracted from \
different layers of a convnet (trained on ImageNet). The general idea \
is to capture color/texture information at different spatial \
scales (fairly large scales --defined by the depth of the layer considered).


- The content loss is a L2 distance between the features of the base \
image (extracted from a deep layer) and the features of the combination image, \
keeping the generated image close enough to the original one. \


Environment
-----------

The code was tested in Python3 with the following dependencies:

- numpy
- scipy
- keras
- tensorflow


References
----------

- `A Neural Algorithm of Artistic Style <http://arxiv.org/abs/1508.06576>`_.
- `Very Deep Convolutional Networks for Large-Scale Image Recognition <https://arxiv.org/abs/1409.1556>`_.
'''

from __future__ import print_function
import numpy as np
from keras.preprocessing.image import load_img, save_img, img_to_array
from keras import backend as K
from scipy.optimize import fmin_l_bfgs_b
import time
import argparse
import ntpath
import os

# VGG19: Fully Trained Convolutional Neural Network with 19 layers.
from keras.applications import vgg19


def get_args():
    '''Argument parser

    Returns
    -------
    namespace: Namespace
        An object to take the attributes.
    '''
    parser = argparse.ArgumentParser(description='Neural art style transfer with python.')
    parser.add_argument('base_image_path', metavar='base', type=str,
                        help='Path to the image to transform.')
    parser.add_argument('style_reference_image_path', metavar='ref', type=str,
                        help='Path to the style reference image.')
    parser.add_argument('result_prefix', metavar='res_prefix', type=str,
                        help='Prefix to append to the input image name for the saved results on every iteration.')
    parser.add_argument('--iter', type=int, default=10, required=False,
                        help='Number of iterations to run.')
    parser.add_argument('--content_weight', type=float, default=0.025, required=False,
                        help='Content weight.')
    parser.add_argument('--style_weight', type=float, default=1.0, required=False,
                        help='Style weight.')
    parser.add_argument('--tv_weight', type=float, default=1.0, required=False,
                        help='Total Variation weight.')
    return parser.parse_args()

# # if executed from the interpreter:
# base_image_path = 'img/input_and_output/input_chicago.jpg'
# style_reference_image_path = 'img/style/munch_thescream.jpg'
# result_prefix = "transfer"
# iterations = 10

# total_variation_weight = 0.025
# style_weight = 1.0
# content_weight = 1.0

# These values work OK too:
# total_variation_weight = 1.0
# style_weight = 5.0
# content_weight = 0.025


def get_dims(base_image_path):
    '''Get the dimensions of the generated picture calculated from the dimensions
    of the original picture, maintaining the ratio.

    Parameters
    ----------
    base_image_path: str
        The path of the image to transform.

    Returns
    -------
    img_nrows: int
        number of rows of the image.
    img_ncols: int
        number of columns of the image.
    '''
    width, height = load_img(base_image_path).size
    img_nrows = 400
    img_ncols = int(width * img_nrows / height)
    return(img_nrows, img_ncols)


# Util function to open, resize and format pictures into appropriate tensors
def preprocess_image(image_path, img_nrows, img_ncols):
    '''Util function to open, resize and format pictures into appropriate
    tensors

    This preprocessing:

    a) Adds a new Dimension to the image in order to concatenate
    it with the other image at a later stage, and

    b) According to 'Very Deep Convolutional Networks for Large-Scale Image
    Recognition' paper, pre-processes the Input Data:

    1. Subtract mean of RGB values of Image from the Content and Style Images.
    This subtraction is done to make data compatible with VGG Neural Network.
    The input images should be zero-centered by mean pixel
    (rather than mean image) subtraction. Namely, the following BGR
    values should be subtracted: [103.939, 116.779, 123.68].
    According to A Neural Network for Artistic Style paper:

    2. Flip the Ordering of Image from RGB (red-green-blue) colorspace
    to BGR (blue-green-red) colorspace as in paper 'A Neural Algorithm
    of Artistic Style'

    Parameters
    ----------
    image_path: str
        The path of the image to preprocess.
    img_nrows: int
        number of rows of the image.
    img_ncols: int
        number of columns of the image.

    Returns
    -------
    img: numpy array
        image in the form of a numpy array.

    '''
    img = load_img(image_path, target_size=(img_nrows, img_ncols))
    img = img_to_array(img)
    # Add a new Dimension to the image in order to concatenate
    # it with the other image at a later stage
    img = np.expand_dims(img, axis=0)
    # According to 'Very Deep Convolutional Networks for Large-Scale Image
    # Recognition' paper, pre-process the Input Data:
    # 1. Subtract mean of RGB values of Image from the Content and Style Images.
    # This subtraction is done to make data compatible with VGG Neural Network.
    # The input images should be zero-centered by mean pixel
    # (rather than mean image) subtraction. Namely, the following BGR
    # values should be subtracted: [103.939, 116.779, 123.68].
    # According to A Neural Network for Artistic Style paper:
    # 2. Flip the Ordering of Image from RGB (red-green-blue) colorspace
    # to BGR (blue-green-red) colorspace as in paper 'A Neural Algorithm
    # of Artistic Style'
    img = vgg19.preprocess_input(img)
    return img


# Util function to convert a tensor into a valid image
# (deprocess the preprocessed image)
def deprocess_image(img, img_nrows, img_ncols):
    '''Util function to convert a tensor into a valid image
    (deprocess the preprocessed image)

    Deprocessing means:

    a) From the VGG paper:
    The following BGR values, subtracted in the preprocessing stage
    (vgg19.preprocess_input), are added: [103.939, 116.779, 123.68].

    b) Deconvert again from BGR to RGB.

    Parameters
    ----------
    img: numpy array
        The image to deprocess.
    img_nrows: int
        number of rows of the image.
    img_ncols: int
        number of columns of the image.

    Returns
    -------
    img: numpy array
        image in the form of a numpy array.

    '''
    x = img
    if K.image_data_format() == 'channels_first':
        x = x.reshape((3, img_nrows, img_ncols))
        x = x.transpose((1, 2, 0))
    else:
        x = x.reshape((img_nrows, img_ncols, 3))
    # From the VGG paper:
    # The following BGR values, subtracted in the preprocessing stage
    # (vgg19.preprocess_input), should be added:
    # [103.939, 116.779, 123.68].
    x[:, :, 0] += 103.939
    x[:, :, 1] += 116.779
    x[:, :, 2] += 123.68
    #  Deconvert again from BGR to RGB
    # 'BGR'->'RGB'
    x = x[:, :, ::-1]
    x = np.clip(x, 0, 255).astype('uint8')
    return x


# Get tensor representations of images
def get_input_tensor(base_image_path, img_nrows, img_ncols, style_reference_image_path):
    '''Get tensor representations of images and concatenate them into one
    input_tensor.

    A.k.a. convert images to arrays with the preprocess_image function
    and convert the arrays to tensors with the Tensorflow Backend.

    a) Create a tensor that represents the Final Output Image
    It is a combination of base_image and style_image
    Dimensions are the same as the base and style images

    b) Combine the 3 images (base, style and combination), now tensors,
    into a single Keras tensor. (axis=0 concatenates Tensors along
    one direction).

    Parameters
    ----------
    base_image_path: str
        The path of the image to transform.
    img_nrows: int
        number of rows of the image.
    img_ncols: int
        number of columns of the image.
    style_reference_image_path: str
        The path of the style image to get the style from.

    Returns
    -------
    input_tensor: Keras tensor
        input_tensor in the form of a Keras (tf) tensor.

    '''
    base_image = K.variable(preprocess_image(base_image_path, img_nrows,
                                             img_ncols))
    style_reference_image = K.variable(preprocess_image(style_reference_image_path,
                                                        img_nrows, img_ncols))

    # Create a tensor that represents the Final Output Image
    # It is a combination of base_image and style_image
    # Dimensions are the same as the base and style images
    if K.image_data_format() == 'channels_first':
        combination_image = K.placeholder((1, 3, img_nrows, img_ncols))
    else:
        combination_image = K.placeholder((1, img_nrows, img_ncols, 3))

    # Combine the 3 images (base, style and combination), now tensors
    # into a single Keras tensor
    # axis=0 concatenates Tensors along one direction
    input_tensor = K.concatenate([base_image,
                                  style_reference_image,
                                  combination_image], axis=0)
    return(input_tensor, combination_image)


def get_model(input_tensor):
    '''Build the VGG19 network

    Buil the CNN with our 3 images as input (more precisely,
    with the input tensor, which is a concatenation of the tensor
    representation of the three images)
    The model will be loaded with pre-trained ImageNet weights.
    This means that the CNN was trained on ImageNet dataset:
    http://www.image-net.org/
    We use the built-in VGG19 from Keras and remove the fully connected layers

    Parameters
    ----------
    input_tensor: Keras tensor
        input_tensor in the form of a Keras (tf) tensor.

    Returns
    -------
    model: Keras model
        CNN model VGG19 from preloaded with weights trained on ImageNet. It does
        not inclued the fully connected layers.

    '''
    model = vgg19.VGG19(input_tensor=input_tensor,
                        weights='imagenet', include_top=False)
    print('Model loaded.')
    return(model)


# The base iamge loss and style image loss needs to be minimized.
# The combination image loss also needs to be minimized.
# Also, the total variation loss also needs to be minimized.
# To do so, we need to define 4 util functions:


# ------------------------------- Style Loss ------------------------------
def gram_matrix(x):
    '''Take the gram matrix of an image tensor.

    The gram matrix of an image tensor (feature-wise outer product)
    results from multiplying a matrix with the transpose of itself.
    We can think of the spatial information that was contained in the
    original representation to have been “distributed”. The Gram matrix
    instead contains non-localized information about the image,such as
    texture, shapes, and weights: style!

    Another explanation:
    Gram Matrix is a matrix whose terms are Covariances of corresponding
    set of features and thus captures the information as which features
    tend to activate together. By only capturing these aggregate
    statistics across the image, they are blind to
    the specific arrangement of objects inside the image.
    This is what allows them to capture information about style
    independent of content of the Image.

    Parameters
    ----------
    x: Keras tensor
        tensor in the form of a Keras (tf) tensor.

    Returns
    -------
    gram: Keras tensor
        Gram matrix of the input tensor.


    '''
    assert K.ndim(x) == 3
    if K.image_data_format() == 'channels_first':
        features = K.batch_flatten(x)
    else:
        features = K.batch_flatten(K.permute_dimensions(x, (2, 0, 1)))
    gram = K.dot(features, K.transpose(features))
    return gram


def style_loss(style, combination):
    '''Style loss function.

    The "style loss" is designed to maintain
    the style of the reference image in the generated image.
    It is based on the gram matrices (which capture style) of
    feature maps from the style reference image
    and from the generated image.
    Mathematically, the style loss is then the (scaled, squared)
    Frobenius norm of the difference between the Gram matrices of the
    style and combination images
    '''
    assert K.ndim(style) == 3
    assert K.ndim(combination) == 3
    S = gram_matrix(style)
    C = gram_matrix(combination)
    channels = 3
    size = img_nrows * img_ncols
    return K.sum(K.square(S - C)) / (4.0 * (channels ** 2) * (size ** 2))


# ------------------------- Content Loss ----------------------------------
def content_loss(base, combination):
    '''Content loss function.

    An auxiliary loss function designed to maintain the "content" of the
    base image in the generated image.
    Mathematically, we draw the content feature from "block5_conv2" layer
    (remember we defined a dict of layers, right?)
    The content loss is the (scaled, squared) Euclidean distance
    between feature representations of the content and combination images.
    '''
    return K.sum(K.square(combination - base))


# --------------------------- Total Variation Loss -----------------------
def total_variation_loss(x):
    '''Total variation loss.

    The two losses computed above produce a noisy output.
    Hence, to smoothen the output, we compute the Total Variation Loss,
    designed to keep the generated image locally coherent
    assert K.ndim(x) == 4
    '''
    if K.image_data_format() == 'channels_first':
        a = K.square(
            x[:, :, :img_nrows - 1, :img_ncols - 1] - x[:, :, 1:, :img_ncols - 1])
        b = K.square(
            x[:, :, :img_nrows - 1, :img_ncols - 1] - x[:, :, :img_nrows - 1, 1:])
    else:
        a = K.square(
            x[:, :img_nrows - 1, :img_ncols - 1, :] - x[:, 1:, :img_ncols - 1, :])
        b = K.square(
            x[:, :img_nrows - 1, :img_ncols - 1, :] - x[:, :img_nrows - 1, 1:, :])
    return K.sum(K.pow(a + b, 1.25))


def get_final_image(loss, combination_image):
    '''Get Final combination image (it will need to be optimized).

    We need to compute the Gram Matrix for each step consisting of
    Maxpooling and ReLU activation Functions.
    Then we need to compute the style loss for Gram Matrix at each step.
    The loss at each step is added to get the final loss at the end.
    '''
    for layer_name in feature_layers:
        layer_features = outputs_dict[layer_name]
        style_reference_features = layer_features[1, :, :, :]
        combination_features = layer_features[2, :, :, :]
        sl = style_loss(style_reference_features, combination_features)
        loss += (style_weight / len(feature_layers)) * sl
    # Calculate the total Variation Loss for the Final Output Image
    # (Combination Image)
    loss = loss + total_variation_weight * total_variation_loss(combination_image)

    # We have computed the Combination Image, the final Output
    # Use BackPropagation:
    # Define gradients of the total loss relative
    # to the combination image, and use these gradients to iteratively
    # improve upon our combination image to minimise the loss.
    # Obtain the gradient of each parameter
    grads = K.gradients(loss, combination_image)

    outputs = [loss]
    if isinstance(grads, (list, tuple)):
        outputs += grads
    else:
        outputs.append(grads)

    return(K.function([combination_image], outputs))


def eval_loss_and_grads(x):
    '''Evaluate Loss and Gradients and send it to optimizer to reduce loss
    and tune Gradients
    '''
    if K.image_data_format() == 'channels_first':
        x = x.reshape((1, 3, img_nrows, img_ncols))
    else:
        x = x.reshape((1, img_nrows, img_ncols, 3))
    outs = f_outputs([x])
    loss_value = outs[0]
    if len(outs[1:]) == 1:
        grad_values = outs[1].flatten().astype('float64')
    else:
        grad_values = np.array(outs[1:]).flatten().astype('float64')
    return loss_value, grad_values


# -------------------------------Optimization -----------------------------
class Evaluator(object):
    '''Evaluator to use with scipy's optimize L-BFGS.

    This Evaluator class makes it possible
    to compute loss and gradients in one pass
    while retrieving them via two separate functions,
    "loss" and "grads". This is done because scipy.optimize
    requires separate functions for loss and gradients,
    but computing them separately would be inefficient.
    scipy.optimize function L-BFGS (https://es.wikipedia.org/wiki/L-BFGS)
    is the recommended optimizer used in the paper
    A Neural Algorithm of Artistic Style
    '''

    def __init__(self):
        self.loss_value = None
        self.grads_values = None

    def loss(self, x):
        assert self.loss_value is None
        loss_value, grad_values = eval_loss_and_grads(x)
        self.loss_value = loss_value
        self.grad_values = grad_values
        return self.loss_value

    def grads(self, x):
        assert self.loss_value is not None
        grad_values = np.copy(self.grad_values)
        self.loss_value = None
        self.grad_values = None
        return grad_values


if __name__ == "__main__":
    args = get_args()
    base_image_path = args.base_image_path
    style_reference_image_path = args.style_reference_image_path
    result_prefix = args.result_prefix
    iterations = args.iter
    total_variation_weight = args.tv_weight
    style_weight = args.style_weight
    content_weight = args.content_weight

    img_nrows, img_ncols = get_dims(base_image_path)
    input_tensor, combination_image = \
        get_input_tensor(base_image_path, img_nrows, img_ncols,
                         style_reference_image_path)
    model = get_model(input_tensor)

    # Make a dict of layers in VGG19 to refer to each "key" layer individually.
    outputs_dict = dict([(layer.name, layer.output) for layer in model.layers])

    # Initialize Total Loss
    loss = K.variable(0.0)
    # Take the tuned features from the "block5_conv2" layer
    layer_features = outputs_dict['block5_conv2']
    # In input_tensor, we concatenated all tensors into one, so
    # [base_image (0), style_image (1), combination_image (2)]
    base_image_features = layer_features[0, :, :, :]
    combination_features = layer_features[2, :, :, :]
    # Total Loss: (content weight x content loss)
    loss = loss + content_weight * content_loss(base_image_features,
                                                combination_features)

    # Feature Layers of VGG19 Trained Neural Network
    feature_layers = ['block1_conv1', 'block2_conv1',
                      'block3_conv1', 'block4_conv1',
                      'block5_conv1']

    f_outputs = get_final_image(loss, combination_image)

    evaluator = Evaluator()

    # We use the base image as starting point for optimization.
    x = preprocess_image(base_image_path, img_nrows, img_ncols)

    # Then, we use L-BFGS algorithm to iteratively tune the Loss and the Gradients.
    # (run scipy-based optimization L-BFGS over the pixels of the generated
    # image to minimize the neural style loss)

    # NOTE: Increase number of iterations to obtain better results

    for i in range(iterations):
        print('Start of iteration', i)
        start_time = time.time()
        x, min_val, info = fmin_l_bfgs_b(evaluator.loss, x.flatten(),
                                         fprime=evaluator.grads, maxfun=20)
        print('Current loss value:', min_val)
        # save current generated image
        img = deprocess_image(x.copy(), img_nrows, img_ncols)
        path_name = os.path.dirname(base_image_path)
        fname = os.path.splitext(ntpath.basename(base_image_path))[0]
        fname = fname + '_' + result_prefix + '_at_iteration_%d.png' % i
        fname = os.path.join(path_name, fname)
        save_img(fname, img)
        end_time = time.time()
        print('Image saved as', fname)
        print('Iteration %d completed in %ds' % (i, end_time - start_time))
