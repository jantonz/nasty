#!/bin/bash
# Run NASTY for several pictures and styles

STYLE_PATHS=(img/style/van_gogh_starry_night.jpg img/style/klint_the_kiss.jpg img/style/kandinsky_composition_vii.jpg img/style/munch_the_scream.jpg img/style/picasso_femme_assise.jpg img/style/picasso_guernica.jpg)

STYLE_NAMES=(van_gogh klint kandinsky munch picasso_femme_assise picasso_guernica)

INPUT_IMAGES=(img/input_and_output/paris.jpeg)

for ((j=0;j<${#INPUT_IMAGES[@]};j++));
do
for ((i=0;i<${#STYLE_NAMES[@]};++i));
do
echo ${INPUT_IMAGES[j]}
echo ${STYLE_PATHS[i]}
echo ${STYLE_NAMES[i]}
python neural_style_transfer.py ${INPUT_IMAGES[j]} ${STYLE_PATHS[i]} ${STYLE_NAMES[i]} --iter 80 --content_weight 0.025 --style_weight 5.0 --tv_weight 1.0;
done;
done


