.. NASTY documentation master file, created by
   sphinx-quickstart on Mon Dec 17 13:05:42 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to NASTY's documentation!
==================================================

.. toctree::
   :maxdepth: 3
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. automodule:: neural_style_transfer
   :members:
