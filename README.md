# NASTY: Neural Art Style Transfer with pYthon.

A small tool to transfer style from one painting to an image.

**Regal de l'Amic Invisible 2018 del Lluís!**

## Examples

|Style image                                                                    |  Transformed image                          |
|-------------------------------------------------------------------------------|---------------------------------------------|
|<img src="img/style/klint_the_kiss.jpg"  width="213" height="213">             | ![](img/gif/nasty_klint.gif)                |
|<img src="img/style/van_gogh_starry_night.jpg"  width="320" height="213">      | ![](img/gif/nasty_van_gogh.gif)             |
|<img src="img/style/kandinsky_composition_vii.jpg"  width="320" height="213">  | ![](img/gif/nasty_kandinsky.gif)            |
|<img src="img/style/munch_the_scream.jpg"  width="213" height="320">           | ![](img/gif/nasty_munch.gif)                |
|<img src="img/style/picasso_femme_assise.jpg"  width="213" height="320">       | ![](img/gif/nasty_picasso_femme_assise.gif) |
|<img src="img/style/picasso_guernica.jpg"  width="320" height="170">           | ![](img/gif/nasty_picasso_guernica.gif)     |

## Usage

Run the script with:

` python neural_style_transfer.py path_to_your_base_image.jpg path_to_your_reference.jpg prefix_for_results `

e.g.:

` python neural_style_transfer.py img/input_output/paris.jpg img/style/starry_night.jpg results/my_result `

Optional parameters:

* ` --iter, To specify the number of iterations the style transfer takes place (Default is 10) `
* ` --content_weight, The weight given to the content loss (Default is 0.025) `
* ` --style_weight, The weight given to the style loss (Default is 1.0) `
* ` --tv_weight, The weight given to the total variation loss (Default is 1.0) `

It is preferable to run this script on GPU, for speed.

## Details

Style transfer consists in generating an image with the same “content” as a base image, but with the “style” of a different picture (typically artistic).

This is achieved through the optimization of a loss function that has 3 components: “style loss”, “content loss”, and “total variation loss”:

*    The total variation loss imposes local spatial continuity between the pixels of the combination image, giving it visual coherence.
*    The style loss is where the deep learning keeps in –that one is defined using a deep convolutional neural network. Precisely, it consists in a sum of L2 distances between the Gram matrices of the representations of the base image and the style reference image, extracted from different layers of a convnet (trained on ImageNet). The general idea is to capture color/texture information at different spatial scales (fairly large scales –defined by the depth of the layer considered).
*    The content loss is a L2 distance between the features of the base image (extracted from a deep layer) and the features of the combination image, keeping the generated image close enough to the original one.

## Environment

The code was tested in Python 3.6 with the following dependencies:

*    numpy
*    scipy
*    keras
*    tensorflow
*    pillow

Just `pip install requirements.txt` in your Python3.6 environment.
Or, if you use conda, `conda create --name <name of your environment> --file requirements_conda.txt`.


## References

* [A Neural Algorithm of Artistic Style.](http://arxiv.org/abs/1508.06576)
* [Very Deep Convolutional Networks for Large-Scale Image Recognition.](https://arxiv.org/abs/1409.1556)
